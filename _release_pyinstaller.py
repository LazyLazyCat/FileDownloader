# -*- coding: utf-8 -*-
"""
Created on Thu Nov 30 16:49:35 2017

@author: gengyixin
"""

#
# release using pyinstaller

import os

if __name__ == '__main__':
	py_script_path = "./FileDownloader.py"
	pyinstaller_command = "pyinstaller -F -w " \
						  "--specpath ./_Release " \
						  "--distpath ./_Release/dist " \
						  "--workpath ./_Release/build "
	os.system(pyinstaller_command + py_script_path)