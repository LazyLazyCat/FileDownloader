# -*- coding: utf-8 -*-
"""
Created on Tue Dec 12 09:42:58 2017

@author: gengyixin
"""


import os, requests, re
from PyQt5.QtWidgets import QMainWindow, QLabel, QLineEdit, QPushButton, QInputDialog, QFileDialog, QMessageBox


class FileDownloader(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("File Downloader")
        self.setFixedSize(400, 180)
        # url
        self.wLabelUrl = QLabel("URL:", self)
        self.wLabelUrl.setGeometry(20, 15, 25, 25)
        self.wUrl = QLineEdit(self)
        self.wUrl.setGeometry(50, 15, 330, 25)
        # search options
        self.wLabelSearchOption = QLabel("Search Options:", self)
        self.wLabelSearchOption.setGeometry(20, 45, 100, 25)
        self.wAddSearchOption = QPushButton("+", self)
        self.wAddSearchOption.setGeometry(120, 45, 25, 25)
        self.wAddSearchOption.clicked.connect(self.AddSearchOption)
        self.wDeleteSearchOption = QPushButton("-", self)
        self.wDeleteSearchOption.setGeometry(150, 45, 25, 25)
        self.wDeleteSearchOption.clicked.connect(self.DeleteSearchOption)
        self.wListSearchOptions = QLineEdit(self)
        self.wListSearchOptions.setReadOnly(True)
        self.wListSearchOptions.setGeometry(180, 45, 200, 25)
        self.wListSearchOptions.setToolTip("如：signal;processing代表查找名含signal或processing的文件")
        self.wLabelExtension = QLabel("Extensions:", self)
        self.wLabelExtension.setGeometry(20, 75, 100, 25)
        self.wAddExtension = QPushButton("+", self)
        self.wAddExtension.setGeometry(120, 75, 25, 25)
        self.wAddExtension.clicked.connect(self.AddExtension)
        self.wDeleteExtension = QPushButton("-", self)
        self.wDeleteExtension.setGeometry(150, 75, 25, 25)
        self.wDeleteExtension.clicked.connect(self.DeleteExtension)
        self.wListExtensions = QLineEdit(self)
        self.wListExtensions.setReadOnly(True)
        self.wListExtensions.setGeometry(180, 75, 200, 25)
        self.wListExtensions.setText("pdf")
        # destination
        self.wLabelDestinationFolder = QLabel("Destination:", self)
        self.wLabelDestinationFolder.setGeometry(20, 105, 100, 25)
        self.wDestinationFolder = QLineEdit(self)
        self.wDestinationFolder.setReadOnly(True)
        self.wDestinationFolder.setGeometry(120, 105, 230, 25)
        self.wDestinationFolder.setText("./Download")
        self.wChangeDestinationFolder = QPushButton("..", self)
        self.wChangeDestinationFolder.setGeometry(355, 105, 25, 25)
        self.wChangeDestinationFolder.clicked.connect(self.ChangeFolder)
        # download
        self.wDownload = QPushButton("Download", self)
        self.wDownload.setGeometry(310, 145, 75, 25)
        self.wDownload.clicked.connect(self.Download)
        self.url = self.wUrl.text()
        self.options = []
        self.extensions = ["pdf"]
        self.destination = "Download"

    def AddSearchOption(self):
        option, ok = QInputDialog.getText(self, "Add Search Option...", "", QLineEdit.Normal)
        if ok and option:
            self.options.append(option)
            text = self.wListSearchOptions.text()
            if text:
                self.wListSearchOptions.setText(text + "; " + option)
            else:
                self.wListSearchOptions.setText(option)
            
    def DeleteSearchOption(self):
        self.options.remove(self.options[-1])
        text = self.wListSearchOptions.text()
        self.wListSearchOptions.setText(text[0:text.rfind(";")])

    def AddExtension(self):
        ext, ok = QInputDialog.getText(self, "Add File Extension...", "", QLineEdit.Normal)
        if ok and ext:
            self.extensions.append(ext)
            self.wListExtensions.setText(self.wListExtensions.text() + "; " + ext)
            
    def DeleteExtension(self):
        self.extensions.remove(self.extensions[-1])
        text = self.wListExtensions.text()
        self.wListExtensions.setText(text[0:text.rfind(";")])

    def ChangeFolder(self):
        dir = QFileDialog.getExistingDirectory(self, "Save to...", options=QFileDialog.ShowDirsOnly)
        if dir:
            self.destination = dir
            self.wDestinationFolder.setText(dir)

    def Download(self):
        self.url = self.wUrl.text()
        if not self.url:
            QMessageBox.warning(self, "Error", "Please specify the page url to search for files.", QMessageBox.Ok)
            return
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36"}
        html = requests.get(self.url, headers=headers).text
        options = ""
        for option in self.options:
            options += option + "|"
        if options:
            options = "(?:" + options[0: -1] + ")+?[^\?\*\/\\\<\>\:\"\|]*?"
        count = 0
        for ext in self.extensions:
            reg = r'(?:href|HREF)="?((?:http://)?.+?%s\.%s)' % (options, ext)
            url_re = re.compile(reg)
            files = re.findall(url_re, html)
            if not files:
                QMessageBox.warning(self, "Warning", "Found no file satisfying.", QMessageBox.Ok)
                return
            if not os.path.exists(self.destination):
                os.mkdir(self.destination)
            os.chdir(os.path.join(os.getcwd(), self.destination))
            self.setWindowTitle("Downloading...")
            for file_url in files:
                if not file_url.startswith("http://"):
                    paths = file_url.split("/")
                    if len(paths) > 1:
                        url = self.url[0: (self.url.find("/", len("http://")) + 1)] + file_url
                    else:
                        url = self.url[0: (self.url.rfind("/") + 1)] + file_url
                code = requests.get(url, headers=headers)
                file_name = paths[-1]
                with open(file_name, 'wb') as f:
                    f.write(code.content)
                    count += 1
        self.setWindowTitle("File Downloader")
        QMessageBox.information(self, "Download Complete!", "Downloaded {} files.".format(count), QMessageBox.Ok)


if __name__ == "__main__":
    # tell windows that appid has been customized
    import ctypes
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID("myappid")
    from PyQt5.QtWidgets import QApplication
    if not QApplication.instance():
        import sys
        app = QApplication(sys.argv)
    else:
        # make sure there will be only one QApplication instance
        app = QApplication.instance()
    window = FileDownloader()
    window.show()
    app.exec()
